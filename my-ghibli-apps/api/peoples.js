import { __apiGet } from '~/utils/api-utils'
import { API_PEOPLE } from '~/configs/api-urls'

const GET_PEOPLE_LIST = $store => __apiGet($store, API_PEOPLE.ROOT)
const GET_PEOPLE_BY_ID = ($store, peopleId) => __apiGet($store, API_PEOPLE.DETAIL(peopleId))

export default {
  GET_PEOPLE_BY_ID,
  GET_PEOPLE_LIST
}