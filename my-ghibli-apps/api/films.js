import { __apiGet } from '~/utils/api-utils';
import { API_FILM } from '~/configs/api-urls';

const GET_FILM_LIST = $store => __apiGet($store, API_FILM.ROOT);
const GET_FILM_BY_ID = ($store, filmId) => __apiGet($store, API_FILM.DETAIL(filmId));

export default {
  GET_FILM_LIST,
  GET_FILM_BY_ID
}
