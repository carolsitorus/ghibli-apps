import { __apiGet } from '~/utils/api-utils';
import { API_VEHICLE } from '~/configs/api-urls';

const GET_VEHICLE_LIST = $store => __apiGet($store, API_VEHICLE.ROOT);
const GET_VEHICLE_BY_ID = ($store, vehicleId) => __apiGet($store, API_VEHICLE.DETAIL(vehicleId));

export default {
  GET_VEHICLE_BY_ID,
  GET_VEHICLE_LIST
}
