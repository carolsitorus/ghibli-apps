const speciesData = [
  {
    "id": "af3910a6-429f-4c74-9ad5-dfe1c4aa04f2",
    "name": "Human",
    "classification": "Mammal",
    "eye_colors": "Black, Blue, Brown, Blue, Grey, Green, Hazel",
    "hair_colors": "Black, Blonde, Brown, Grey, White",
    "people": [],
    "films": [],
    "url": "https://ghibliapi.herokuapp.com/species/af3910a6-429f-4c74-9ad5-dfe1c4aa04f2"
  }
]

export default speciesData