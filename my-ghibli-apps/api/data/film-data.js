const filmData = [
  {
    "id": "2baf70d1-42bb-4437-b551-e5fed5a87abe",
    "title": "dummy-title",
    "description": "dummy-description",
    "director": "dummy-director",
    "producer": "dummy-producer",
    "release_date": "dummy-release-date",
    "rt_score": "dummy-rt-score",
    "people": [
      'dummy-people'
    ],
    "species": [
      'dummy-species'
    ],
    "locations": [
      'dummy-locations'
    ],
    "vehicles": [
      'dummy-vehicles'
    ],
  }
]

export default filmData