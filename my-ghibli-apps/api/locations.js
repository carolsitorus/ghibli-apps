import { __apiGet } from '~/utils/api-utils'
import { API_LOCATION } from '~/configs/api-urls'

const GET_LOCATION_LIST = $store => __apiGet($store, API_LOCATION.ROOT)
const GET_LOCATION_BY_ID = ($store, locationId) => __apiGet($store, API_LOCATION.DETAIL(locationId))

export default {
  GET_LOCATION_BY_ID,
  GET_LOCATION_LIST
}