import { __apiGet } from '~/utils/api-utils';
import { API_SPECIES } from '~/configs/api-urls';

const GET_SPECIES_LIST = $store => __apiGet($store, API_SPECIES.ROOT);
const GET_SPECIES_BY_ID = ($store, speciesId) => __apiGet($store, API_SPECIES.DETAIL(speciesId));

export default {
  GET_SPECIES_BY_ID,
  GET_SPECIES_LIST
}
