import Vuex from 'vuex'
import film from './modules/film'
import location from './modules/location'
import people from './modules/people'
import species from './modules/species'
import vehicle from './modules/vehicle'

const createStore = () =>
  new Vuex.Store({
    modules: {
      film,
      location,
      people,
      species,
      vehicle
    },
  });

export default createStore;
