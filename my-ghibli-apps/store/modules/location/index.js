import apiLocation from '~/api/locations'

export const state = () => ({
  locationDetail: {},
  locationList: []
})

export const getters = {
  locationList: (state) => {
    return state.locationList
  },
  locationDetail: (state) => {
    return state.locationDetail
  }
}

export const mutations = {
  setLocationDetail: (state, data) => {
    state.locationDetail = data
  },
  setLocationList: (state, data) => {
     state.locationList = data
   }
}

export const actions = {
  getLocationById( {commit}, locationId) {
    apiLocation.GET_LOCATION_BY_ID(this, locationId)
    .then( function (response) {
      commit('setLocationDetail', response.data)
    })
  },
  getLocationList ( {commit}) {
    apiLocation.GET_LOCATION_LIST (this)
    .then( function (response) {
      commit('setLocationList', response.data)
    })
  },
}

export default {
  actions,
  getters,
  mutations,
  state
}