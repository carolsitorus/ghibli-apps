import apiPeople from '~/api/peoples';

export const state = () => ({
  peopleDetail: {},
  peopleList: [],
})

export const getters = {
  peopleList: (state) => {
    return state.peopleList
  },
  peopleDetail: (state) => {
    return state.peopleDetail
  }
}

export const mutations = {
  setPeopleDetail (state, data) {
    state.peopleDetail = data
  },
  setPeopleList (state, data) {
    state.peopleList = data
  }
}

export const actions = {
  getPeopleById ({commit}, peopleId) {
    apiPeople.GET_PEOPLE_BY_ID(this, peopleId)
    .then( function (response) {
      commit('setPeopleDetail', response.data)
    })
  },
  getPeopleList ({commit}) {
    apiPeople.GET_PEOPLE_LIST(this)
    .then(function (response) {
      commit('setPeopleList', response.data)
    })
    .catch(function (error) {
    });
  }
}

export default {
  actions,
  getters,
  mutations,
  state
}


