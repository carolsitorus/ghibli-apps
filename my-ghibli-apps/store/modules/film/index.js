import apiFilm from '~/api/films'
import { sortAscByTitle } from '~/utils/sort-utils'
export const state = () => ({
  inputQuery: '',
  filmDetail: {},
  filmList: [],
})

export const getters = {
  filmList: (state) => {
    return state.filmList
  },
  filmDetail: (state) => {
    return state.filmDetail
  }
}

export const mutations = {
  inputMutations (state, data) {
    state.inputQuery = data
  },
  setFilmList (state, data) {
    state.filmList = data
  },
  setFilmDetail (state, data) {
    state.filmDetail = data
  }
}

export const actions = {
  getFilmList ({commit}) {
    apiFilm.GET_FILM_LIST(this)
    .then(function (response) {
      let data = response.data
      data = sortAscByTitle(data)
      commit('setFilmList', data)
    })
    .catch(function (error) {
    });
  },
  getFilmById ({commit}, filmId) {
    apiFilm.GET_FILM_BY_ID(this, filmId)
    .then( function (response) {
      commit('setFilmDetail', response.data)
    })
  }
}

export default {
  actions,
  getters,
  mutations,
  state
}


