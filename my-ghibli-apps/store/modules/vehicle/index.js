import apiVehicle from '~/api/vehicles';

export const state = () => ({
  vehicleDetail: {},
  vehicleList: [],
})

export const getters = {
  vehicleList: (state) => {
    return state.vehicleList
  },
  vehicleDetail: (state) => {
    return state.vehicleDetail
  }
}

export const mutations = {
  setVehicleDetail (state, data) {
    state.vehicleDetail = data
  },
  setVehicleList (state, data) {
    state.vehicleList = data
  }
}

export const actions = {
  getVehicleById ({commit}, peopleId) {
    apiVehicle.GET_VEHICLE_BY_ID(this, peopleId)
    .then( function (response) {
      commit('setVehicleDetail', response.data)
    })
  },
  getVehicleList ({commit}) {
    apiVehicle.GET_VEHICLE_LIST(this)
    .then(function (response) {
      commit('setVehicleList', response.data)
    })
    .catch(function (error) {
    });
  }
}

export default {
  actions,
  getters,
  mutations,
  state
}


