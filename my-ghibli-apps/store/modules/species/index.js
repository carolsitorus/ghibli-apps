import apiSpecies from '~/api/species';

export const state = () => ({
  speciesDetail: {},
  speciesList: [],
})

export const getters = {
  speciesDetail: (state) => {
    return state.speciesDetail
  },
  speciesList: (state) => {
    return state.speciesList
  }
}

export const mutations = {
  setSpeciesDetail (state, data) {
    state.speciesDetail = data
  },
  setSpeciesList (state, data) {
    state.speciesList = data
  }
}

export const actions = {
  getSpeciesById ({commit}, filmId) {
    apiSpecies.GET_SPECIES_BY_ID(this, filmId)
    .then( function (response) {
      commit('setSpeciesDetail', response.data)
    })
  },
  getSpeciesList ({commit}) {
    apiSpecies.GET_SPECIES_LIST(this)
    .then(function (response) {
      commit('setSpeciesList', response.data)
    })
    .catch(function (error) {
    });
  }
}

export default {
  actions,
  getters,
  mutations,
  state
}


