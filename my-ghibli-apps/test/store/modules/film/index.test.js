import film from '~/store/modules/film';
import filmData  from '~/api/data/film-data'

describe('store modules/film/index', () => {
  const filmList = filmData
  
  test('setFilmList should mutate succesfully', () => {
    const state = {
      filmList: []
    };

    const expectedState = {
      filmList
    };

    film.mutations.setFilmList(state, filmList);
    expect(state).toEqual(expectedState);
    expect(film.getters.filmList(state)).toEqual(expectedState.filmList);
  })
});