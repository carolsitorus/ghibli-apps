import apiFilm from '~/api/films'
import moxios from 'moxios';
import testApi from '~/test/helpers/test-api-basic'
import apiData from '~/api/data/film-data'

const dummyStore = {
  state: {},
};

const dummyId = apiData[0].id

beforeEach(() => {
  moxios.install()
})

afterEach(() => {
  moxios.uninstall()
})

describe('api films', () => {
  test('GET_FILM_LIST should sucessfully', (done) => {
    const testMe = () => apiFilm.GET_FILM_LIST(dummyStore)
    testApi.generateBasicTest(testMe, done)
  })

  test('GET_FILM_BY_ID should sucessfully', (done) => {
    const testMe = () => apiFilm.GET_FILM_BY_ID(dummyStore, dummyId)
    testApi.generateBasicTest(testMe, done)
  })
})