import apiLocation from '~/api/locations'
import moxios from 'moxios';
import testApi from '~/test/helpers/test-api-basic'
import apiData from '~/api/data/location-data'

const dummyStore = {
  state: {},
};

const dummyId = apiData[0].id

beforeEach(() => {
  moxios.install()
})

afterEach(() => {
  moxios.uninstall()
})

describe('api locations', () => {
  test('GET_LOCATION_LIST should sucessfully', (done) => {
    const testMe = () => apiLocation.GET_LOCATION_LIST(dummyStore)
    testApi.generateBasicTest(testMe, done)
  })

  test('GET_LOCATION_BY_ID should sucessfully', (done) => {
    const testMe = () => apiLocation.GET_LOCATION_BY_ID(dummyStore, dummyId)
    testApi.generateBasicTest(testMe, done)
  })
})