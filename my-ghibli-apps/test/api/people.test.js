import apiPeople from '~/api/peoples'
import moxios from 'moxios';
import testApi from '~/test/helpers/test-api-basic'
import apiData from '~/api/data/people-data'

const dummyStore = {
  state: {},
};

const dummyId = apiData[0].id

beforeEach(() => {
  moxios.install()
})

afterEach(() => {
  moxios.uninstall()
})

describe('api peoples', () => {
  test('GET_PEOPLE_LIST should sucessfully', (done) => {
    const testMe = () => apiPeople.GET_PEOPLE_LIST(dummyStore)
    testApi.generateBasicTest(testMe, done)
  })

  test('GET_PEOPLE_BY_ID should sucessfully', (done) => {
    const testMe = () => apiPeople.GET_PEOPLE_BY_ID(dummyStore, dummyId)
    testApi.generateBasicTest(testMe, done)
  })
})