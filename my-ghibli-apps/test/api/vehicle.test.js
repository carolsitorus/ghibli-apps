import apiVehicle from '~/api/vehicles'
import moxios from 'moxios';
import testApi from '~/test/helpers/test-api-basic'
import apiData from '~/api/data/vehicle-data'

const dummyStore = {
  state: {},
};

const dummyId = apiData[0].id

beforeEach(() => {
  moxios.install()
})

afterEach(() => {
  moxios.uninstall()
})

describe('api species', () => {
  test('GET_SPECIES_LIST should sucessfully', (done) => {
    const testMe = () => apiVehicle.GET_VEHICLE_LIST(dummyStore)
    testApi.generateBasicTest(testMe, done)
  })

  test('GET_SPECIES_BY_ID should sucessfully', (done) => {
    const testMe = () => apiVehicle.GET_VEHICLE_BY_ID(dummyStore, dummyId)
    testApi.generateBasicTest(testMe, done)
  })
})