import sortUtil from '~/utils/sort-utils'

const dummyParam = [
  {title: 'dummy-def'},
  {title: 'dummy-abc'},
  {title: 'dummy-ghi'},
  {title: 'dummy-title'},
  {title: 'dummy-title'}
]

const expectedOutput = [
  {title: 'dummy-abc'},
  {title: 'dummy-def'},
  {title: 'dummy-ghi'},
  {title: 'dummy-title'},
  {title: 'dummy-title'}
]

describe('utils sort-utils', () => {
  test('sortAscByTitle should successfully', () => {
    const result = sortUtil.sortAscByTitle(dummyParam)
    expect(result).toEqual(expectedOutput);
  })
})