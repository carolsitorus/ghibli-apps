import { createLocalVue, shallowMount } from '@vue/test-utils'
import Component from '~/pages/locations/_id'
import locationData  from '~/api/data/location-data'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)
let wrapper = null

const $route = {
  path: '/locations/' + locationData[0].id,
  params: {
    id: locationData[0].id
  }
}

const store = new Vuex.Store({
  state: {
    location: {
      locationDetail: []
    }
  },
  mutations: {
    setLocationDetail (state, data) {
      state.location.locationDetail = data
    }
  },
  actions: {
    getLocationById(context) {
      let response = locationData[0]
      context.commit('setLocationDetail', response)
    }
  }
})

describe('pages locations/_id', () => {
  wrapper = shallowMount(Component, {
    store,
    localVue,
    mocks: {
      $route
    }
  });

  test('Render detail url successfully', () => {
    expect(wrapper).toBeTruthy();
    expect(wrapper.vm.$route.path).toEqual('/locations/' + locationData[0].id)
  })

  test('Renders locationDetail using a mock store successfully', () => {
    wrapper.vm.$store.dispatch('getLocationDetail', wrapper.vm)
    expect(wrapper.vm.$store.state.location.locationDetail).toEqual(locationData[0])
  })
})

