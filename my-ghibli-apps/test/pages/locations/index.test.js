import { createLocalVue, RouterLinkStub, shallowMount } from '@vue/test-utils'
import Component from '~/pages/locations/index'
import locationData  from '~/api/data/location-data'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)
let wrapper = null

const store = new Vuex.Store({
  state: {
    location: {
      locationList: []
    }
  },
  mutations: {
    setLocationList (state, data) {
      state.location.locationList = data
    }
  },
  actions: {
    getLocationList(context) {
      let response = locationData
      context.commit('setLocationList', response)
    }
  }
})

describe('pages locations/index', () => {
  wrapper = shallowMount(Component, {
    store,
    localVue,
    stubs: {
      NuxtLink: RouterLinkStub,
    },
  });

  test('Render pages locations/index successfully', () => {
    expect(wrapper).toBeTruthy();
  })

  test('Render locationList using a mock store successfully', () => {
    wrapper.vm.$store.dispatch('getLocationList', wrapper.vm)
    expect(wrapper.vm.$store.state.location.locationList).toEqual(locationData)
  })

  test('Render pages locations/_id sucessfully', () => {
    const links = wrapper.findAll(RouterLinkStub)
    expect(links.at(0).props().to).toEqual('/locations/'+ locationData[0].id)
  })
})
