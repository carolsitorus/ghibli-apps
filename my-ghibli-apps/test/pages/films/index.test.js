import { createLocalVue, RouterLinkStub, shallowMount } from '@vue/test-utils'
import Component from '~/pages/films/index'
import filmData  from '~/api/data/film-data'
import FilmDetailPage from '~/pages/films/_id'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)
let wrapper = null

const store = new Vuex.Store({
  state: {
    film: {
      filmList: []
    }
  },
  mutations: {
    setFilmList (state, data) {
      state.film.filmList = data
    }
  },
  actions: {
    getFilmList(context) {
      let response = filmData
      context.commit('setFilmList', response)
    }
  }
})

describe('pages films/index', () => {
  wrapper = shallowMount(Component, {
    store,
    localVue,
    stubs: {
      NuxtLink: RouterLinkStub,
    },
  });

  test('Render pages film/index successfully', () => {
    expect(wrapper).toBeTruthy();
  })

  test('Render filmList using a mock store successfully', () => {
    wrapper.vm.$store.dispatch('getFilmList', wrapper.vm)
    expect(wrapper.vm.$store.state.film.filmList).toEqual(filmData)
  })

  test('Render pages film/_id sucessfully', () => {
    const links = wrapper.findAll(RouterLinkStub)
    expect(links.at(0).props().to).toEqual('/films/'+ filmData[0].id)
  })
})
