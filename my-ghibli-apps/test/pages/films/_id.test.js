import { createLocalVue, mount } from '@vue/test-utils'
import Component from '~/pages/films/_id'
import filmData  from '~/api/data/film-data'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)
let wrapper = null

const $route = {
  path: '/films/' + filmData[0].id,
  params: {
    id: filmData[0].id
  }
}

const store = new Vuex.Store({
  state: {
    film: {
      filmDetail: []
    }
  },
  mutations: {
    setFilmDetail (state, data) {
      state.film.filmDetail = data
    }
  },
  actions: {
    getFilmById(context) {
      let response = filmData[0]
      context.commit('setFilmDetail', response)
    }
  }
})

describe('pages films/_id', () => {
  wrapper = mount(Component, {
    store,
    localVue,
    mocks: {
      $route
    }
  });

  test('Render detail url successfully', () => {
    expect(wrapper).toBeTruthy();
    expect(wrapper.vm.$route.path).toEqual('/films/' + filmData[0].id)
  })

  test('Renders filmDetail using a mock store successfully', () => {
    wrapper.vm.$store.dispatch('getFilmDetail', wrapper.vm)
    expect(wrapper.vm.$store.state.film.filmDetail).toEqual(filmData[0])
  })
})

