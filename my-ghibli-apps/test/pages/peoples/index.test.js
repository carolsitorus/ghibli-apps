import { createLocalVue, RouterLinkStub, shallowMount } from '@vue/test-utils'
import Component from '~/pages/peoples/index'
import peopleData  from '~/api/data/people-data'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)
let wrapper = null

const store = new Vuex.Store({
  state: {
    people: {
      peopleList: []
    }
  },
  mutations: {
    setPeopleList (state, data) {
      state.people.peopleList = data
    }
  },
  actions: {
    getPeopleList(context) {
      let response = peopleData
      context.commit('setPeopleList', response)
    }
  }
})

describe('pages peoples/index', () => {
  wrapper = shallowMount(Component, {
    store,
    localVue,
    stubs: {
      NuxtLink: RouterLinkStub,
    },
  });

  test('Render pages peoples/index successfully', () => {
    expect(wrapper).toBeTruthy();
  })

  test('Render peopleList using a mock store successfully', () => {
    wrapper.vm.$store.dispatch('getPeopleList', wrapper.vm)
    expect(wrapper.vm.$store.state.people.peopleList).toEqual(peopleData)
  })

  test('Render pages peoples/_id sucessfully', () => {
    const links = wrapper.findAll(RouterLinkStub)
    expect(links.at(0).props().to).toEqual('/peoples/'+ peopleData[0].id)
  })
})
