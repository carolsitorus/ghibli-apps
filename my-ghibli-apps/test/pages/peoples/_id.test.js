import { createLocalVue, shallowMount } from '@vue/test-utils'
import Component from '~/pages/peoples/_id'
import peopleData  from '~/api/data/people-data'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)
let wrapper = null

const $route = {
  path: '/peoples/' + peopleData[0].id,
  params: {
    id: peopleData[0].id
  }
}

const store = new Vuex.Store({
  state: {
    people: {
      peopleDetail: []
    }
  },
  mutations: {
    setPeopleDetail (state, data) {
      state.people.peopleDetail = data
    }
  },
  actions: {
    getPeopleById(context) {
      let response = peopleData[0]
      context.commit('setPeopleDetail', response)
    }
  }
})

describe('pages peoples/_id', () => {
  wrapper = shallowMount(Component, {
    store,
    localVue,
    mocks: {
      $route
    }
  });

  test('Render detail url successfully', () => {
    expect(wrapper).toBeTruthy();
    expect(wrapper.vm.$route.path).toEqual('/peoples/' + peopleData[0].id)
  })

  test('Renders peopleDetail using a mock store successfully', () => {
    wrapper.vm.$store.dispatch('getPeopleDetail', wrapper.vm)
    expect(wrapper.vm.$store.state.people.peopleDetail).toEqual(peopleData[0])
  })
})

