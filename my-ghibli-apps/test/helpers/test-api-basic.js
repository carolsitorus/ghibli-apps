/* eslint-env jest */
import moxios from 'moxios';

export default {
  generateBasicTest: (API_FUNC_CALL, done, mockResponse, extraExpect) => {
    API_FUNC_CALL();

    const responseParam = mockResponse || 'OK';

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: responseParam,
      }).then((response) => {
        expect(response.status).toEqual(200);
        if (responseParam === 'OK') {
          expect(response.data).toEqual('OK');
        } else {
          expect(response.data).toEqual(mockResponse);
        }

        if (extraExpect) extraExpect(response);

        done();
      });
    });
  }
};
