import axios from 'axios';
import { METHOD } from '~/configs/constants';

export const __apiGet = ($store, URL, params) => axios({
  url: URL,
  method: METHOD.GET,
  params,
});