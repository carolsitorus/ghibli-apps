export const sortAscByTitle = (data) => {
  data.sort(function(a, b){
    if(a.title < b.title) return -1
    if(a.title > b.title) return 1
    return 0
  })
  return data
}

export default {
  sortAscByTitle,
};