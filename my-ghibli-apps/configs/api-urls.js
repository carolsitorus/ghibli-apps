const BASE_URL = `${process.env.API_URL}`


const BASE_LOCATION = `${BASE_URL}/locations`
export const API_LOCATION = {
  ROOT: `${BASE_LOCATION}`,
  DETAIL: locationId => `${BASE_LOCATION}/${locationId}`
};

const BASE_FILM = `${BASE_URL}/films`
export const API_FILM = {
  ROOT: `${BASE_FILM}`,
  DETAIL: filmId => `${BASE_FILM}/${filmId}`
};

const BASE_PEOPLE = `${BASE_URL}/people`
export const API_PEOPLE = {
  ROOT: `${BASE_PEOPLE}`,
  DETAIL: peopleId=> `${BASE_PEOPLE}/${peopleId}`
};

const BASE_SPECIES = `${BASE_URL}/species`
export const API_SPECIES = {
  ROOT: `${BASE_SPECIES}`,
  DETAIL: speciesId => `${BASE_SPECIES}/${speciesId}`
};

const BASE_VEHICLE = `${BASE_URL}/vehicles`
export const API_VEHICLE = {
  ROOT: `${BASE_VEHICLE}`,
  DETAIL: vehicleId => `${BASE_VEHICLE}/${vehicleId}`
};

export default {
  API_LOCATION,
  API_FILM,
  API_PEOPLE,
  API_SPECIES,
  API_VEHICLE
}